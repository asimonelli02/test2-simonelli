//Antonio Simonelli
public class PayrollManagement {

	public static void main(String[] args) {
		Employee[] employees = new Employee[5];
		employees[0] = new SalariedEmployee(1000);
		employees[1] = new SalariedEmployee(1200);
		employees[2] = new SalariedEmployee(1400);
		employees[3] = new HourlyEmployee(40, 10);
		employees[4] = new UnionizedHourlyEmployee(40, 10, 5000);

	}

	public double getTotalExpenses(Employee[] totalExpense) {
		double amount = 0;
		for (int i = 0; i < totalExpense.length; i++) {
			amount += totalExpense[i].getYearlyPay();
		}
		return amount;
	}

}
