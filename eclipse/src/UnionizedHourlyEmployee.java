//Antonio Simonelli
public class UnionizedHourlyEmployee extends HourlyEmployee {
	private double pensionContribution;

	public UnionizedHourlyEmployee(double hoursWorked, double hourlyPay, double pensionContribution) {
		super(hoursWorked, hourlyPay);
		this.pensionContribution = pensionContribution;
	}

	public double getHourlyPay() {
		return super.getYearlyPay() + this.pensionContribution;
	}
}
