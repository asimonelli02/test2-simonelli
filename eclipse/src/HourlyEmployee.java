//Antonio Simonelli
public class HourlyEmployee implements Employee {
	private double hoursWorked;
	private double hourlyPay;

	public HourlyEmployee(double hoursWorked, double hourlyPay) {
		this.hoursWorked = hoursWorked;
		this.hourlyPay = hourlyPay;
	}

	public double getYearlyPay() {
		double yearlyPay = this.hoursWorked * this.hourlyPay * 52;
		return yearlyPay;
	}
}
