//Antonio Simonelli
public class SalariedEmployee implements Employee {
	private double yearlySalary;

	public SalariedEmployee(double yearlySalary) {
		this.yearlySalary = yearlySalary;
	}

	public double getYearlyPay() {
		return this.yearlySalary;
	}
}
